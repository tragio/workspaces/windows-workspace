# Workspace for Windows

## Overview

As Microsoft has continued to pivot in a direction that supports (and even
fuses) Linux and Windows environments, it is more possible than ever before to
expose a seamless Linux development environment "natively" from a Windows host.

As a developer of a linux application, library, web application, or Docker
container, consider the following requirements:

- A linux shell and filesystem on which to do development.
- A linux Docker daemon to run OCI containers.
- A linux environment from which to use applications like Vagrant, Packer, and
Terraform to drive activity on Windows Host Hypervisors.
- An ability to run VMs with nested virtualization enabled (for Android
development, to run a nested hypervisor, etc.)

As of 20H2, Windows has introduced some new features which finally make ALL
of this possible in a single location.

## Pre-requisites

- Intel or AMD Processor with virtualization extensions enabled (e.g. i5-i9, Ryzen)
- Windows 10 Professional or Enterprise, **updated to 20H2**

## Host Configuration

Important note... almost every "function" here enables an aspect of Microsoft
Hyper-V.  While it has come a long way in terms of sharing real estate with
other hypervisors, if you require certain features (i.e. nested virtualization)
in another hypervisor (e.g. VMware workstation), enabling Hyper-V will still
break that feature.  That said, being able to run Hyper-V adjacent to VMware
Workstation and VirtualBox without them breaking each other is pretty awesome.

### To Do Near-Native Linux Development...

The following steps will enable the Windows Subsystem for Linux (WSL) and then
enable just enough of Microsoft Hyper-V to transparently deploy a Linux
Service VM adjacent to Windows.  This is managed so tightly by the OS that if
you were to look at Hyper-V Manager, while you will see the network adapter for
WSL, you will not actually see the VM listed.  Regardless, this represents a
significant improvement over the first version of WSL, as it provides Linux
native filesystem and prevents file permission issues at write time and also
enables this Linux environment to interact with critical Windows host services
for the best of both worlds.

#### From Administrator Powershell

See: https://docs.microsoft.com/en-us/windows/wsl/install-win10

```
# If you run into issus re: execution policy, here is lazy solution...
# Set-ExecutionPolicy Bypass -Scope CurrentUser -Force

# Windows Feature: "Windows Subsystem for Linux"
Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux
# Windows Feature "Virtual Machine Platform
Enable-WindowsOptionalFeature -Online -FeatureName VirtualMachinePlatform

# Reboot

# Update the kernel for WSL (substitute "arm64" for "x64" as required)
Start-Process msiexec.exe -ArgumentList "/i `"https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi`" /quiet" -Wait

# Install Linux Distro of Interest (Ubuntu 20.04)
Invoke-WebRequest -Uri https://aka.ms/wslubuntu2004 -OutFile distro.appx -UseBasicParsing
Add-AppxPackage .\distro.appx

PS> enable wsl2
PS> wsl --set-default-version 2

```

Alt:

`dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart`
`dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart`


### To Do Docker Development using the above

Install Docker for Windows.
Enable integration with WSL 2.
Do NOT "install" docker in WSL... that is all managed.

### To Use VirtualBox/VMWare on Host CONCURRENTLY with any Hyper-V Feature Enabled

AWESOME new feature provided by 20H2...user mode VMM.

See: https://blogs.vmware.com/workstation/2020/05/vmware-workstation-now-supports-hyper-v-mode.html

Enables VirtualBox and VMware to run alongside Hyper-V.

```
# Windows Feature: "Windows Hypervisor Platform"
Enable-WindowsOptionalFeature -Online -FeatureName HypervisorPlatform
```

### To Use Microsoft Hyper-V to Run VMs

```
# Windows Feature: "Hyper-V"
Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V -All
```

Alt:

`dism.exe /Online /Enable-Feature:Microsoft-Hyper-V /All`

IF NEED TO UNINSTALL, USE DISM... PS is incomplete...

Memory Ballooning is nice...but remember, if you are using nested virt, this will
disable ballooning and lock-in startup RAM.  This is an important nuance not to
miss!  Basically, do not frivolously enable nested virt on Hyper-V.

`Set-VMProcessor -VMName <VMName> -ExposeVirtualizationExtensions $true`

### To Run Packer from WSL 2 to control VirtualBox

With VirtualBox installed on Windows, Hypervisor Platform enabled, install
Packer in WSL:

```
wget https://releases.hashicorp.com/packer/1.6.6/packer_1.6.6_linux_amd64.zip
unzip packer_1.6.6_linux_amd64.zip
sudo mv packer /usr/bin/local
sudo ln -s "/mnt/c/Program Files/Oracle/VirtualBox/VBoxManage.exe" /usr/local/bin/VBoxManage
```

### To Run Vagrant from WSL 2 to control VirtualBox

With VirtualBox installed on Windows, Hypervisor Platform enabled, install
Vagrant in WSL:

```
sudo apt install vagrant
echo 'export VAGRANT_WSL_ENABLE_WINDOWS_ACCESS="1"' >> .bashrc
vagrant plugin install winrm winrm-elevated
vagrant up --provider virtualbox
```
